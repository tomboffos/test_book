<?php

namespace App\Controller;

use App\Entity\Book;
use App\Repository\BookRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class BookController extends AbstractController
{

    /**
     * @Route("/books", name="book_list", methods={"GET"})
     */
    public function index(BookRepository $bookRepository, EntityManagerInterface $entityManager): JsonResponse
    {
        $request = Request::createFromGlobals();


        $page =  $request->query->get('page') ?  $request->query->get('page') - 1 : 0;
        $limit = $request->query->get('limit') ?? 10;

        $offset = $page * $limit;

        if ($request->query->get('name'))
            $query = "SELECT * FROM book WHERE book.name LIKE '{$request->query->get('name')}'
                  OR  book.author  LIKE '{$request->query->get('name')}' OFFSET {$offset} ROWS";
        else
            $query = "SELECT * FROM book OFFSET {$offset} ROWS";

        $statement = $entityManager->getConnection()->prepare($query);


        $statement->execute();


        return new JsonResponse($statement->executeQuery()->fetchAll());
    }


    /**
     * @Route("/authors", name="authors", methods={"GET"})
     */
    public function authors(EntityManagerInterface $entityManager)
    {
        $request = Request::createFromGlobals();

        $page =  $request->query->get('page') ?  $request->query->get('page') - 1 : 0;
        $limit = $request->query->get('limit') ?? 10;

        $offset = $page*$limit;

        $query = "SELECT COUNT(book.id) as book_count,book.author FROM book GROUP BY author OFFSET {$offset} ROWS";

        $statement = $entityManager->getConnection()->prepare($query);


        $statement->execute();
        return new JsonResponse($statement->executeQuery()->fetchAll());
    }

    /**
     * @Route("/books/random", name="book_rand",methods={"GET"})
     */

    public function rand(BookRepository $bookRepository)
    {
        $books = $bookRepository->findAll();

        $rand = mt_rand(0, count($books) - 1);

        return new JsonResponse($books[$rand]->toJson(), 200);
    }

    /**
     * @Route("/books/store", name="book_store", methods={"POST"})
     */
    public function create(BookRepository $bookRepository, ValidatorInterface $validator): JsonResponse
    {
        $request = Request::createFromGlobals();

        $name = $request->query->get('name');
        $author = $request->query->get('author');

        $book = new Book();

        if ($name)
            $book->setName($name);

        if ($author)
            $book->setAuthor($author);


        if (count($validator->validate($book)) > 0) {
            return $this->json([
                'error' => (string)$validator->validate($book)
            ]);
        }
        $bookRepository->add($book, true);

        return $this->json($bookRepository->findOneBy(['id' => $book->getId()])->toJson(), 201);
    }


    /**
     * @Route("/books", name="books_update",methods={"PUT"})
     */
    public function put(EntityManagerInterface $entityManager): JsonResponse
    {
        $request = Request::createFromGlobals();

        $book = $entityManager->getRepository(Book::class)->findOneBy(['id' => $request->query->get('id')]);
        $name = $request->query->get('name');
        $author = $request->query->get('author');

        if (!$book)
            return $this->json(['message' => 'Not found'], 404);

        if ($name)
            $book->setName($name);

        if ($author)
            $book->setAuthor($author);

        $entityManager->flush();

        return new JsonResponse($book->toJson(), 200);
    }


    /**
     * @Route("/books/{id}", name="book_show",methods={"GET"})
     */
    public function show(int $id, BookRepository $bookRepository): JsonResponse
    {

        $book = $bookRepository->findOneBy(['id' => $id]);
        if (!$book)
            return $this->json(['message' => 'Not found'], 404);


        return new JsonResponse($book->toJson());
    }


    /**
     * @Route("/books/delete/{id}", name="book_update",methods={"GET"})
     */
    public function delete(int $id, EntityManagerInterface $entityManager): JsonResponse
    {
        $book = $entityManager->getRepository(Book::class)->findOneBy(['id' => $id]);
        if (!$book)
            return $this->json(['message' => 'Not found'], 404);

        $entityManager->remove($book);

        $entityManager->flush();


        return new JsonResponse([], 200);
    }


}
